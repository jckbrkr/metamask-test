window.addEventListener('load', function() {

  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.log('web3 injected');
  } else {
    console.log('no web3');
    $('#warning').removeClass('d-none');
    $('#warning').html("Please install the <a title=\"MetaMask plugin\" href=\"https://metamask.io/\">MetaMask plugin<a>");

  }

  // Check if user is logged in by looking at MetaMask accounts
  web3.eth.getAccounts(function(err, accounts){
      if (err != null) {
        console.error("An error occurred: "+err);
      } else if (accounts.length == 0) {
        console.log("User is not logged in to MetaMask");
        $('#warning').removeClass('d-none');
        $('#warning').html("You are not logged in to MetaMask");
      } else {
        console.log("User is logged in to MetaMask");
        $('#success').removeClass('d-none');
        $('#success').html("You are logged in to MetaMask!");
        accounts.forEach(function(account) {
            $('#user').append(account);
        });
        console.log(accounts);
      }
  });


})
